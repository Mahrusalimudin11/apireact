<?php

namespace App\Http\Controllers;

use App\Models\inews;
use Illuminate\Http\Request;

class InewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\inews  $inews
     * @return \Illuminate\Http\Response
     */
    public function show(inews $inews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\inews  $inews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, inews $inews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\inews  $inews
     * @return \Illuminate\Http\Response
     */
    public function destroy(inews $inews)
    {
        //
    }
}
